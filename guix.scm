;; Setup a development environment for Dungeon Maid
;; Note that this simply uses the latest Python
;; TODO: use specifically Python 3.6

(use-modules (gnu packages))

(packages->manifest
 (map specification->package
      '("git"
        "jupyter"
        "python"
        "python-black@21.12b0"
        "python-pytest")))
