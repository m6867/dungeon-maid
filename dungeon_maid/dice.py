"""
The set of functions for rolling and manipulating dice in Dungeon Maid
"""

# Standard library imports

from itertools import takewhile
from random import randint
from typing import List

# Dependency imports

from dataclasses import dataclass


class DieRoll(int):
    """Class for recording the results of individual die rolls"""

    original_value: int = 0
    dropped: bool = False
    sides: int

    def __new__(cls, sides):
        cls.sides = sides
        return super(DieRoll, cls).__new__(cls, randint(1, sides))

    def reroll(self):
        new_roll = DieRoll(self.sides)
        new_roll.original_value = self
        return new_roll


@dataclass
class RollResult:
    """Class for returning the results of a roll"""

    query: str
    result: int
    roll_outcome: List[DieRoll]
    description: str = ""  # This is optional


# Informal type definitions

_rules = ["k", "d", "r", "ro"]  # type definition and handy global
"""
A RollRule is a str in _rules optionally followed by an int > 0, n.
Each RollRule has the following meaning:
- "k": keep the highest n dice
- "d": drop the highest n dice
- "r": reroll dice of value n or lower
- "ro": reroll up to 1 di of value n or lower

A RollCommand is a str with the following format:
    - number: int > 0
    - d: str == "d"
    - sides: int in [4, 6, 8, 10, 12, 20, 100]
    - rule (optional): str in _rules
    - n (optional): int
    - mod (optional - requires bonus): str == "+" or str == "-"
    - bonus (optional): int > 0
Examples:
    - "2d20k1+10"
    - "2d6r2+5"
    - "5d8d3+1"
    - "10d10ro4-5"
    - "1d20+5"
    - "2d4k"

A RollResult is a dataclass (defined above)
"""

# Functions


def roll(roll_cmd):
    """
    Evaluate a RollCommand; return a RollResult; report errors only
    """
    try:
        return roll_with_errors(roll_cmd)
    except ValueError as e:
        print(f"Error: {e}")


def roll_with_errors(roll_cmd):
    """
    Evaluate a RollCommand; return a RollResult; throw on errors
    """

    rest = roll_cmd.replace(" ", "")
    res_roll = RollResult("", 0, [])
    res_sign = ""

    try:
        while rest:

            rule = None
            n = 1
            mod = 0

            dice, sides, rest = _extract_sides_dice(rest)

            if rest and _is_potential_rule(rest[0]):
                rule, n, rest = _extract_rule_n(rest)

            cnt_sign = lambda l: l.count("+") + l.count("-")
            if rest and (not "d" in rest or cnt_sign(rest[: rest.index("d")]) > 1):
                mod, rest = _extract_modifier(rest)

            outcome, result = enumerated_roll(dice, sides, rule, n, mod)

            result = (
                res_roll.result - result
                if res_sign == "-"
                else res_roll.result + result
            )
            outcome = res_roll.roll_outcome + outcome

            res_roll = RollResult(query=roll_cmd, result=result, roll_outcome=outcome)

            if "d" in rest:
                res_sign = rest[0]
                rest = rest[1:]
            else:
                res_sign = ""

    except ValueError as e:
        orig_rest = roll_cmd.replace(" ", "")
        raise ValueError(
            f"{e} in {orig_rest} at index {orig_rest.index(rest)} (0-indexed)"
        ) from None

    return res_roll


def enumerated_roll(dice, sides, rule, n, mod):
    """
    Roll dice dice of sides sides and manipulate the result
    according to rule and n; return the list of kept rolls and their sum
    """
    outcome = [DieRoll(sides) for _ in range(dice)]
    result = sum(outcome)

    if rule == None:
        pass
    elif rule == "k":
        for i in sorted(outcome, reverse=True)[n:]:
            i.dropped = True
        result = sum(filter(lambda r: not r.dropped, outcome))
    elif rule == "d":
        for i in sorted(outcome, reverse=True)[0:n]:
            i.dropped = True
        result = sum(filter(lambda r: not r.dropped, outcome))
    elif rule == "r":
        outcome = list(map(lambda r: r.reroll() if r <= n else r, outcome))
        result = sum(outcome)
    elif rule == "ro":
        reroll_val = min(outcome)
        if reroll_val <= n:
            outcome[outcome.index(reroll_val)].reroll()
        result = sum(outcome)

    return outcome, result + mod


def _extract_sides_dice(roll_cmd):
    """
    Extract the number of dice and their sides from a RollCommand;
    return them and the rest of the RollCommand
    """
    dice, rest = _extract_decimal(roll_cmd)
    rest_test = rest
    sides, rest = _extract_decimal(rest[1:])
    if rest_test[0] != "d" or rest == rest_test:
        raise ValueError(f"invalid roll command {roll_cmd}")
    return dice, sides, rest


def _extract_rule_n(s):
    """
    Extract the rule and optional n for it from a RollCommand;
    return them and the rest of the RollCommand
    """
    rule = _accumulate(s, _is_potential_rule)
    n, rest = _extract_decimal(s[len(rule) :])
    if not rule in _rules:
        raise ValueError(f"invalid rule {rule}")
    return rule, n, rest


def _is_potential_rule(c):
    """
    Check if the character c is potentially part of a valid rule
    """
    return c != "+" and c != "-" and not c.isdecimal()


def _extract_modifier(s):
    """Extract a modifier from s"""
    mod_sign = s[0]
    mod, rest = _extract_decimal(s[1:])
    if rest == s[1:] or (mod_sign != "-" and mod_sign != "+"):
        raise ValueError(f"invalid modifier {mod_sign}{mod}")
    return mod if mod_sign != "-" else -mod, rest


def _extract_decimal(s):
    """
    Extract a decimal value from the beginning of a string;
    return it and the rest of the string
    """
    acc = _accumulate(s, lambda v: v.isdecimal())
    if acc == "":
        return 1, s
    else:
        return int(acc), s[len(acc) :]


def _accumulate(s, t):
    """Extract values according from s to t"""
    return "".join(takewhile(t, s))
