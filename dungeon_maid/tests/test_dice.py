from dungeon_maid import dice
import operator

_sum_results = lambda a: sum([i.value for i in a.roll_outcome])


def test_roll():
    """Test basic roll functionality"""
    roll_result = dice.roll("3d6")
    assert len(roll_result.roll_outcome) == 3
    assert 3 <= roll_result.result <= 18
    assert roll_result.result == sum(roll_result.roll_outcome)

    roll_result = dice.roll("10d20")
    assert len(roll_result.roll_outcome) == 10
    assert 10 <= roll_result.result <= 200
    assert roll_result.result == sum(roll_result.roll_outcome)

    roll_result = dice.roll("1d4")
    assert len(roll_result.roll_outcome) == 1
    assert 1 <= roll_result.result <= 4
    assert roll_result.result == sum(roll_result.roll_outcome)


def test_roll_zero_dice():
    "Test rolling no dice"
    roll_result = dice.roll("0d4")
    assert len(roll_result.roll_outcome) == 0
    assert roll_result.result == 0
    assert roll_result.result == _sum_results(roll_result)


"""
    - "2d20k1+10"
    - "2d4k"
    - "2d6r2+5"
    - "5d8d3+1"
    - "10d10ro4-5"
    - "1d20+5"
"""


def test_roll_keep():
    "Test rolling with a rule to keep the highest n dice"
    roll_result = dice.roll("2d4k")
    assert len(roll_result.roll_outcome) == 2
    assert len([i for i in roll_result.roll_outcome if i.dropped]) == 1
    assert 1 <= roll_result.result <= 4
    assert roll_result.result == max(roll_result.roll_outcome)

    roll_result = dice.roll("2d20k1+10")
    assert len(roll_result.roll_outcome) == 2
    assert len([i for i in roll_result.roll_outcome if i.dropped]) == 1
    assert 11 <= roll_result.result <= 31

    roll_result = dice.roll("4d6k3")
    assert len(roll_result.roll_outcome) == 4
    kept_values = [i for i in roll_result.roll_outcome if i.dropped == False]
    dropped_values = [i for i in roll_result.roll_outcome if i.dropped == True]
    assert len(kept_values) == 3
    assert len(dropped_values) == 1
    assert min(kept_values) >= max(dropped_values)


def test_roll_drop():
    """Test rolling with a rule to drop the highest n dice"""
    roll_result = dice.roll("2d4d")
    assert len(roll_result.roll_outcome) == 2
    assert len([i for i in roll_result.roll_outcome if i.dropped]) == 1
    assert 1 <= roll_result.result <= 4
    assert roll_result.result == min(roll_result.roll_outcome)

    roll_result = dice.roll("2d20d1+10")
    assert len(roll_result.roll_outcome) == 2
    assert len([i for i in roll_result.roll_outcome if i.dropped]) == 1
    assert 11 <= roll_result.result <= 31
    assert roll_result.result == min(roll_result.roll_outcome) + 10

    roll_result = dice.roll("4d6d3")
    assert len(roll_result.roll_outcome) == 4
    kept_values = [i for i in roll_result.roll_outcome if i.dropped == False]
    dropped_values = [i for i in roll_result.roll_outcome if i.dropped == True]
    assert len(kept_values) == 1
    assert len(dropped_values) == 3
    assert max(kept_values) <= min(dropped_values)


def test_roll_reroll():
    "Test rolling with a rule to reroll dice below n"
    for _ in range(10):
        roll_result = dice.roll("2d4r")
        assert 2 <= roll_result.result <= 8
        assert len(roll_result.roll_outcome) == 2
        rerolled = [i for i in roll_result.roll_outcome if i.dropped]
        assert len(rerolled) <= 2
        if len(rerolled) > 0:
            assert rerolled[0] == 1

        roll_result = dice.roll("2d4r2")
        assert 2 <= roll_result.result <= 8
        assert len(roll_result.roll_outcome) == 2
        rerolled = [i for i in roll_result.roll_outcome if i.dropped]
        assert len(rerolled) <= 2
        if len(rerolled) > 0:
            assert all(map(lambda x: x <= 2, rerolled))


def test_roll_reroll_one():
    """Test rolling with rule to reroll a die below n"""
    for _ in range(10):
        roll_result = dice.roll("2d4ro")
        assert 2 <= roll_result.result <= 8
        assert len(roll_result.roll_outcome) == 2
        rerolled = [i for i in roll_result.roll_outcome if i.dropped]
        assert len(rerolled) <= 1
        if len(rerolled) > 0:
            assert rerolled[0] == 1

        roll_result = dice.roll("2d4ro2")
        assert 2 <= roll_result.result <= 8
        assert len(roll_result.roll_outcome) == 2
        rerolled = [i for i in roll_result.roll_outcome if i.dropped]
        assert len(rerolled) <= 1
        if len(rerolled) > 0:
            assert rerolled[0] <= 2


def test_roll_subtract():
    """Test subtracting modifiers"""
    roll_result = dice.roll("3d6-1")
    assert 2 <= roll_result.result <= 17
    assert len(roll_result.roll_outcome) == 3
    assert roll_result.result == sum(roll_result.roll_outcome) - 1


def test_roll_complex():
    """Test roll with multiple sets of dice at once"""
    t1 = dice.roll("1d10+1d4")
    assert 2 <= t1.result <= 14
    assert len(t1.roll_outcome) == 2
    assert t1.result == sum(t1.roll_outcome)

    t2 = dice.roll("2d10k+1d4")
    assert 2 <= t2.result <= 14
    assert len(t2.roll_outcome) == 3

    t3 = dice.roll("3d10k2-1+1d4")
    assert 2 <= t3.result <= 23
    assert len(t3.roll_outcome) == 4

    t4 = dice.roll("3d10k2-1+2d4r+1")
    assert 2 <= t4.result <= 28
    assert len(t4.roll_outcome) == 5

    t5 = dice.roll("1d6+1d8-1d4")
    assert -2 <= t5.result <= 13
    assert len(t5.roll_outcome) == 3

    t6 = dice.roll("1d6+1d8-1d4+1d4")
    assert -5 <= t6.result <= 17
    assert len(t6.roll_outcome) == 4
