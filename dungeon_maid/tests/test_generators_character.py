"""
test_generator_character.py - tests for dungeon_maid/generator/character.py
"""

from dungeon_maid.generators import character


def test_character():
    char = character.Character()
    assert len(char.name) > 0
    assert len(char.pronouns) > 0
    assert len(char.age) > 0
    assert len(char.race) > 0

    human_char = character.Character(race="Human")
    assert len(human_char.name.split()) == 1

    half_orc_char = character.Character(race="Half-Orc")
    assert len(half_orc_char.name.split()) == 1

    dwarf_char = character.Character(race="Dwarf")
    assert len(dwarf_char.name.split()) > 1
