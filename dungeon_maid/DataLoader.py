import re
from typing import Union
from dungeon_maid.tables import table


class Node:
    def getAttr(self, str: str):
        if hasattr(self, str):
            return self.__getattribute__(str)
        else:
            return None

    def getAttrType(self, str: str) -> type:
        if hasattr(self, str):
            return type(self.__getattribute__(str))
        else:
            return None


class DataLoader:
    def clean_string(self, str: str) -> str:
        return re.sub(r"\W+", "_", str).lower()

    def ingest(self, src):
        retVal = {}
        if type(src) == dict:
            # do the recursive dict stuff
            n = Node()
            for k, v in src.items():
                if k == "tables":
                    t = self._list_to_dict([table(i) for i in v])
                    n.__setattr__(k, t)
                else:
                    n.__setattr__(k, self.ingest(v))
            return n
        elif type(src) == list:
            # do the recusrive list stuff
            l = []
            for i in src:
                l.append(self.ingest(i))
            return self._list_to_dict(l)
        else:
            # This is neither a dict nor a list so we can just return it.
            return src

    def _list_to_dict(self, l: list) -> Union[list, dict]:
        """Convert the list to a dict if one of the possible labels is available otherwise return the list"""
        labels = ["name", "option"]
        for label in labels:
            if all(hasattr(i, label) for i in l):
                print(f"triggered for {label}! for {l}")
                n = Node()
                for v in l:
                    k = self.clean_string(v.__getattribute__(label))
                    v.__delattr__(label)
                    n.__setattr__(k, v)
                return n
        return l

    def ingest_obj(self, src_dict, target_obj: Node = None) -> Node:
        if target_obj == None:
            target_obj = Node()
        for k, v in src_dict.items():
            if type(v) == dict:
                n = Node()
                o = self.ingest_obj(n, v)
                if target_obj.getAttrType(k) == dict:
                    target_obj.__getattribute__()
                else:
                    target_obj.__setattr__(k, o)
            if type(v) == list:
                if target_obj.getAttrType(k) == list:
                    target_obj.append()

            else:  # Strings, ints, etc
                target_obj.__setattr__(k, v)
        return target_obj


def quick_load(file_name):
    import json
    import os

    d = DataLoader()
    with open(os.path.join("./data/", file_name)) as f:
        t = json.load(f)
    return d.ingest(t)
