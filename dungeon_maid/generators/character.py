"""
character.py - base class for creating and manipulating characters
"""

import json

from random import choice

from ..dice import roll


class Character:
    """
    A basic character sketch

    By itself, a Character should be used to generate NPCs on-the-fly. However,
    it should also serve as a base class for more specific or complex types of
    characters.

    An Character needs the following information:
    * name
    * age
    * race
    * gender presentation/pronouns
    """

    with open("data/names.json") as fp:
        _names_obj = json.load(fp)["name"]
    _races = [opt["name"] for opt in _names_obj]
    _ages = [
        "infant",
        "child",
        "adolescent",
        "young adult",
        "adult",
        "middle-aged",
        "elderly",
    ]
    _pronouns = ["fae/faer", "he/him", "she/her", "they/them", "xe/xim"]

    def __init__(self, race="", name="", age="", pronouns=""):
        """
        Initialize an NPC

        Args:
          race: str - the character's race. note not all races have a name table
          name: str - the character's name
          age: str - a description of the character's age
          pronouns: str - the character's pronouns in '[subject]/[object]' format
        """
        # !!! choose_name relies on race, pronouns, and age being assigned
        self.race = race if race != "" else choice(self._races)
        self.pronouns = pronouns if pronouns != "" else choice(self._pronouns)
        self.age = age if age != "" else choice(self._ages)
        self.name = name if name != "" else self.choose_name()

    def __repr__(self):
        """
        Represent a Character nicely at print

        Subclasses should override this.
        """
        return f"Name: {self.name}\nPronouns: {self.pronouns}\nAge: {self.age}\nRace: {self.race}"

    def choose_name(self):
        """
        Select a name from among this NPC's race's names corresponding to
        race, pronouns, and age
        """
        name_gender = (
            "Female"
            if self.pronouns == "she/her"
            else "Male"
            if self.pronouns == "he/him"
            else choice(["Female", "Male"])
        )
        for race in self._names_obj:
            if race["name"] == self.race:
                all_names = race["tables"]
                break

        if self.race == "Elf" and self.age in "infant child adolescent":
            first_names = [name["result"] for name in all_names[0]["table"]]
        elif self.race == "Lizardfolk":
            first_names = [name["result"] for name in all_names[0]["table"]]
        else:
            first_names = []
            for table in all_names:
                if name_gender in table["option"]:
                    first_names += [name["result"] for name in table["table"]]

        clan_names = []
        for table in all_names:
            if "Clan" in table["option"]:
                clan_names += [name["result"] for name in table["table"]]

        first_name = choice(first_names)
        clan_name = choice(clan_names) if len(clan_names) > 0 else ""

        return first_name + " " + clan_name if clan_name != "" else first_name
