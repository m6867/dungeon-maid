import dungeon_maid.dice as dice


class table:
    def __init__(self, table_dict: dict = None):
        if table_dict is not None:
            self._load_dict(table_dict)

    def _load_dict(self, table_dict: dict):
        for k, v in table_dict.items():
            if k == "table":
                v = list(map(lambda e: table_entry(**e), v))
            self.__setattr__(k, v)

    def _table_filter(self, r):
        return lambda i: i.min <= r <= i.max

    def roll(self):
        result = dice.DieRoll(self.diceType)
        print(f"Rolled {self.diceType}, got {result}")
        value = next(filter(self._table_filter(result), self.table))
        print(f"table value: {value.result}")
        return value


class table_entry:
    def __init__(self, min, max, result):
        self.min = min
        self.max = max
        self.result = result
