"""
Utilities for manipulating JSON data for Dungeon Maid
"""

import json

from os.path import join


# Useful internal globals

_datadir = "data/"
_suffix = ".json"
_indent = "\t"
_seps = (", ", ": ")

# Functions


def load(fn):
    """
    Load the JSON file "data/<fn>.json"
    """
    with open(join(_datadir, fn + _suffix)) as fp:
        return json.load(fp)


def replace(obj, fn):
    """
    Replace the contents of "data/<fn>.json" with obj, creating it if needed
    """
    _save(obj, fn, "w")


def save(obj, fn):
    """
    Save obj to "data/<fn>.json" if it does not exist
    """
    _save(obj, fn, "x")


def _save(obj, fn, mode):
    """
    Save obj to "data/<fn>.json", according to mode
    """
    with open(join(_datadir, fn + suffix), mode) as fp:
        json.dump(obj, fp, indent=_indent, separators=_seps, sort_keys=True)


def insert(obj, fn, key):
    """
    Add obj to "data/<fn>.json" at key
    """
    acc = load(fn)
    acc[key] = obj
    replace(acc, fn)


def update(obj, fn):
    """
    Append to or replace keys in "data/<fn>.json" with those of obj
    """
    acc = load(fn)
    acc.update(obj)
    replace(acc, fn)
