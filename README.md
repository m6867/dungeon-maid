# dungeon-maid

A project aimed at creating a set of tools, in the form of a Python library, which can facilitate running TTRPGs such as Dungeons & Dragons using a real time literate programming paradigm

## Getting started

Dependencies are managed using [Pipenv](https://pipenv.pypa.io/en/latest/). If you have Pipenv installed, you can start working with this project inside of Jupyter Notebook by running the following commands.

``` sh
pipenv install
pipenv run jupyter notebook
```

## Developer Tools

We have `black` as our code linter and `pytest` for running tests. These can be run through pipenv:

``` sh
pipenv install --dev
pipenv run black
pipenv run pytest
```

If you use the Guix package manager, you can also use the `guix.scm` file to easily setup a development environment.
Assuming the path to the project root is in `~/.config/guix/shell-authorized-directories`:

``` sh
guix shell # [[-C | --container] | [--pure [--check]]]
guix shell -m guix.scm -- black
guix shell -m guix.scm -- pytest
```

Note that this simply uses the latest version of Python in Guix.

### Pytest

Tests are located in the `./dungeon_maid/tests/` directory. An automated job will run them on commit.
